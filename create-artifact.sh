#!/bin/bash
set -e
set -x
my_directory=$(pwd -P)

## ragconnect
( cd ~/git/jastadd/ragconnect && git archive -o ${my_directory}/repo_ragconnect.tar dev )
( cd ~/git/jastadd/ragconnect/pages && mkdocs build && tar cf ${my_directory}/pages_ragconnect.tar ../public/* )

## minimal example
( cd ~/git/jastadd/minimal-ragconnect && git archive -o ${my_directory}/repo_minimal_example.tar master )
( cd ~/git/jastadd/minimal-ragconnect/pages && mkdocs build && tar cf ${my_directory}/pages_minimal_example.tar ../public/* )

## this repository
mv models2022.zip models2022.zip.bck || echo "artefact already moved"
tar cf repo_models2022.tar  --exclude=models2022.zip --exclude='*.tar' --exclude-vcs --exclude-vcs-ignores --exclude='token.txt' --exclude='submodule-sheet.ods' --exclude='upload.*' .

## containered
docker save models2022_web:latest > models2022_web.tar
docker save eclipse-mosquitto:latest > models2022_mosquitto.tar
docker save models2022_rag_place:latest > models2022_rag_place.tar
docker save models2022_coordinator:latest > models2022_coordinator.tar
docker save models2022_ros_place:latest > models2022_ros_place.tar

## this workspace
tar cf workspace.tar docker-compose.yml config/* robotic-controller/vnc/*
tar rf workspace.tar --no-recursion shared_directory_images ros3rag web-ros3rag coordinator

## bundle everything together
zip models2022 \
    workspace.tar \
    repo_models2022.tar \
    repo_ragconnect.tar \
    pages_ragconnect.tar \
    repo_minimal_example.tar \
    pages_minimal_example.tar \
    models2022_web.tar \
    models2022_mosquitto.tar \
    models2022_rag_place.tar \
    models2022_coordinator.tar \
    models2022_ros_place.tar \
    README.md \
    links.md
