# The tool RagConnect

Repository: <https://git-st.inf.tu-dresden.de/jastadd/ragconnect>

Documentation: <https://jastadd.pages.st.inf.tu-dresden.de/ragconnect> (master) and <https://jastadd.pages.st.inf.tu-dresden.de/ragconnect-dev> (dev)

# Minimal Examples

Repository: <https://git-st.inf.tu-dresden.de/jastadd/ragconnect-minimal>

Documentation: <https://jastadd.pages.st.inf.tu-dresden.de/ragconnect-minimal>

# Case Study of the Paper ("ros3rag")

Repositories
- RAG part: <https://git-st.inf.tu-dresden.de/jastadd/ros3rag>
- ROS part: <https://git-st.inf.tu-dresden.de/ceti/ros/ccf>
- Web UI: <https://git-st.inf.tu-dresden.de/jastadd/web-ros3rag>
- Coordinator: <https://git-st.inf.tu-dresden.de/jastadd/coordinator>
- Complete workspace: <https://git-st.inf.tu-dresden.de/ceti/ros/models2022>

# Other Used Tools

Visualization of Grammars: <https://jastadd.pages.st.inf.tu-dresden.de/grammar2uml/>

Visualization of ASTs: <https://jastadd.pages.st.inf.tu-dresden.de/relast2uml/>

RelAST: <https://jastadd.pages.st.inf.tu-dresden.de/relational-rags/>

JastAdd: <http://jastadd.org/>
