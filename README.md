# Artifact for the paper "Incremental Causal Connection for Self-Adaptive Systems Based on Relational Reference Attribute Grammars"

Also available at <https://www.doi.org/10.5281/zenodo.6881226>.

## Installation and Preparation of the Case Study

To quickly run the graphical example applications, Docker is used.
Please note that the setup uses vnc and thus offers limited graphics performance.

- Install Docker and docker-compose for your system (for Windows and Mac OS, install Docker Desktop)
- For artifact evaluation (prepared docker images):
    - Unpack the provided archive
    - Load the five docker images (they require around 7GB in total)
        - `docker load -i models2022_web.tar`
        - `docker load -i models2022_mosquitto.tar`
        - `docker load -i models2022_rag_place.tar`
        - `docker load -i models2022_coordinator.tar`
        - `docker load -i models2022_ros_place.tar`
    - Verify, that the docker images have loaded correctly by running `docker images` and check for the four names `models2022_rag_place`, `models2022_coordinator`, `models2022_ros_place`, `models2022_web` and `eclipse-mosquitto` (all with tag `latest`)
    - Create a new directory `models2022` and unpack the archive `workspace.tar` there. It contains all (remaining) necessary parts for the docker-compose build. Please ensure, that there are empty directories `ros3rag`, `web-ros3rag` and `coordinator`. Otherwise docker-compose will produce an error.
- From source code (clone):
    - Clone this repo and its submodules (**Important**: Use `--recurse-submodules`):
        - `git clone --recurse-submodules https://git-st.inf.tu-dresden.de/ceti/ros/models2022.git models2022`
        - Change into the workspace `cd models2022`
        - Prepare/Build all docker images: `docker-compose build` (this will take a while)
- From source code (packaged repository):
    - Extract the repository `tar xf repo_models2022.tar`
    - Prepare/Build all docker images: `docker-compose build` (this will take a while)

## Description of the Expected Outcome of the Case Study

This case study showcases two systems (sites).
Site A as a self-aware virtual environment and site B as a self-adaptive robotic system.
Site A has a runtime model causally connected to the virtual environment in which the human operator performs pick-and-place tasks.
**In this demonstrator** site A has a fixed goal state (that some human operator might have once defined).
Site B has a runtime model causally connected to the physical robot arms and the movable objects.
Additionally, site A and B are causally connected, too.
All causal connections are realized using our approach, as depicted in fig. 5 in the paper.

This case study involves multiple industrial robot arms collaborating to move items to positions defined in site A.
Both sites A and B contain movable objects and regions identified by some id, and described by pose (position, rotation) and size.
The relative spatial positions of regions at site A do not have to match those in site B; in fact, site A might look vastly different from site B.

**In this demonstrator** all blue object are to be moved together in a small region that can only have two objects inside it, and all green and red objects must be moved to some region around the second robot.
As some (green and red) object are near the first robot, both robots need to collaborate to move them to their goal region.
The collaboration is secured by a collaboration zone, in which only one robot can operate at a time.
This is shown by the transparent orange box between the two robots, which "blocks" the robot from entering the collaboration zone.
In other words, if the box is not visible, the robot is allowed to enter.
This is controlled by the runtime model.

## Running the Case Study

- Start the following containers (best to use different terminal tabs/windows):
    - `docker-compose up web`
        - You should see a log output, that connection to MQTT was successful (`Connected to mosquitto at <datetime>`). Otherwise, check that the docker-compose service `mosquitto` is up and running (it should start automatically once the service `web` is started)
        - **Note**: Please avoid refreshing the web UI (e.g., by pressing F5 or Cmd+R), as all intermediate state is lost in that case
    - `docker-compose up coordinator`
    - `docker-compose up rag_place_a`
    - `docker-compose up rag_place_b`
        - There will some log output, and a final `Published some bytes to coordinating/rag-a/status` for place A
        - Place B should start right away, because the coordinator will send a start message to it, thus the last message should be `coordinating/rag-b/status ready`
        - The coordinator described in the paper is included in this artifact, but with limited capabilities as running it as a docker container prohibits it from starting other docker containers easily. The coordinator must be started before all `rag*` and `ros*` places.
    - `docker-compose up ros_place_b1`
    - `docker-compose up ros_place_b2`
        - This will only start the desktop environment(s) and not (yet) the robotic controller(s)
- Open the Web-UI for the RAG application
    - <http://localhost:8050/>
    - This UI offers various controls (only a few are needed to control the case study), a filtered MQTT log, and a second tab to view the runtime model (see below)
    - **Just in case the coordinator is not working, skip otherweise:** Start the RAG application for place B, hit "Start" in the section "*Commands Place B*".
        - It is important, that the RAG application of place B is started *before* the robotic controllers (`run1.sh` and `run2.sh`, see below). The robotic controllers send their initial scene only once, and without receiving it, the RAG application is missing information about the objects in place B.
        - You should then see a message with "ready" for the topic "coordinating/rag-b/status" (in a line like `[<datetime> @ coordinating/rag-b/status ] ready`)
- Open VNC using your browser to start the two robotic controllers
    - <http://localhost:6081/> (First robot)
    - <http://localhost:6082/> (Second robot)
    - You should see a desktop environment with four desktop entries
    - Right-click on "ros_ws" desktop entry -> "Open in terminal". A terminal window should open in directory "~/Desktop"
    - Start the controller using `./run1.sh` (for the first robot) and `./run2.sh` (for the second robot)
    - You should see some log output in the terminal and the visualization program (RViz) for the respective robot should open
    - In RViz you can set the "Frame rate" from the configured "30" to some lower value (e.g., "5") in order to lower CPU utilization. The main window of RViz is divided into a left side with properties, and a right side with the simulated robot. On the left side under 'Displays -> Frame Rate' you can change the value by clicking the current value (e.g. 30), enter a new value and hitting Enter.
    - Once both robotic controllers are up and running, the case study begins and you should see the first commands sent in the Web-UI for the RAG application. From then on, observe the mqtt messages sent and/or the robots moving
- The complete case study takes about 3 min to finish
- **Just in case the coordinator is not working, skip otherweise:** Back in the Web-UI for the RAG application
    - You should see messages for the topics `/ceti_cell_placeworld/scene/update` and `/ceti_cell_2_placeworld/scene/update`
    - Start the RAG application for place A, hit "Start" in the section "*Commands Place A*"
    - Then the case study begins, you should see a "ready" for the topic `coordinating/rag-a/status` and the first commands sent. Also the two robots should start to move executing their command.
- View the RAG runtime model
    - While the case study runs or after it is finished, you can view the runtime state in the tab named "*SVG Model B*"
    - After starting the RAG application for place B, the initial state is displayed
    - To "request" a new model, hit the button "Model" in section "*Commands Place B*". This will both print the model (in the lower MQTT log) and create a visualization shown in the tab "*SVG Model B*". Only the latest model will be visible in the tab "*SVG Model B*"

## Documentation in the Archive

For the minimal example and for the tool RagConnect, the documentation pages have been included in the archive.
To view them, extract the archive (`minimal_example_pages.tar` or `ragconnect_pages.tar`) and start a local webserver in the extract directory `public` to view them (for single pages a browser is sufficient, but for navigation a webserver is recommended).
As an example, a simple webserver is part of the Python installation and can be started using `python -m SimpleHTTPServer 8000` (Python 2) or `python3 -m http.server` (Python 3).
Alternatively, you can view the documentation online using the links provided in `links.md`.

## Experiment with RagConnect Minimal Example

To experiment with a less complex scenario and verify the functionality of the tool RagConnect itself, you can use the minimal example provided in the archive `minimal_example.tar` and at <https://git-st.inf.tu-dresden.de/jastadd/ragconnect-minimal>.
It is not dockerized, and requires a Java VM (tested with version 11) to run.
All three demos require a running MQTT broker on your machine (the application will abort if no connection is possible).
We recommend [mosquitto](https://mosquitto.org/) as it is easy to set up.

Execute `./gradlew run` to start and choose one of the three demos by typing 1, 2, or 3 (and hitting Enter).
Please refer to the documentation (in the archive `minimal_example_pages.tar` or at <https://jastadd.pages.st.inf.tu-dresden.de/ragconnect-minimal>).

## Verifying Results of the Publication

Several important results can be checked for:

### Feasability of the Case Study

- By executing the case study, and seeing the robots move in the correct manner and order, one can verify that the RAG application computes and sends the correct commands, and (indirectly) that the causal connections update the model correctly

### Lines of code of the Case Study

- There is a script using `cloc` to get LOC numbers.
- Either start a new container with the image of a RAG place, e.g., `docker run --rm -it models2022_rag_place_b /bin/bash`
- Or attach to a running container after starting the case study with docker-compose. To attach, run `docker-compose exec rag_place_b /bin/bash`.
- In both cases, you should have a terminal in the directory `/ros3rag`
- Change directory to the `cloc` subdirectory, `cd cloc`
- Execute the script to show lines of code, `./run-cloc.sh`

### Ratio of messages skipped in the Case Study

- When running the case study, every time the model of place B is requested (by hitting the button "Model" in the rightmost section "*Commands Place B*"), information about the processing of received and sent messages is shown as "evaluationCounters" in form of a csv file
- After those information, the ratio is also printed (the ratio between the sum of all skipped messages and the sum of all processed messages)

### Feasability of the tool RagConnect

Using the provided minimal example, the functionality of the tool RagConnect and its functionality can be assessed for the given and for new input specifications.

## FAQ

### The SVG Model B tab never shows anything

There are some possible sources of errors.

(1) The RAG application fails to write the SVG.
This would be visible as a Java exception in the log output of `docker-compose up rag_place_b`.
The directory `/ros3rag/images` has to exist in the container and must be writeable.
Fixing this is not easy, a possibility is to attach to the running container of `rag_place_b` and make sure, that the directory `/ros3rag/images` is writeable.

(2) The SVG was written but is not readable for the web UI.
Everytime an SVG is received, its original and rewritten path is logged for the container of `docker-compose up web`.
As an alternative, you can manually view these images, since they reside on your host machine in the volume `shared_images`.

### Error: build path [...] either does not exist, is not accessible, or is not a valid URL

When running `docker-compose`, it still tries to access the directories where the Dockerfiles are located.
Therefore, at least these directories need to exist.
Normally, they are included in the `workspace.tar`.
If not, please create empty directories `ros3rag` and `web-ros3rag` as siblings of `docker-compose.yml`.

### What are the supported OS/docker versions?

We have tested the artifact on Unix and Windows with the standard `docker`, `docker-compose` and `Docker Desktop`.
Other docker implementations like `podman` are known to have issues and are not supported.
Using the repository directly on Windows, there may be issues with line-endings as within containers Unix line-endings are expected.
